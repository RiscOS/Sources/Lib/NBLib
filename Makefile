# Copyright 1998 Acorn Computers Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# File:    Makefile
# Purpose: Makefile for NBLib

COMPONENT = NBLib
LIBRARIES = ${LIBRARY} ${LIBRARYZM} ${LIBRARYD}
HDRS      =   \
  NBLib       \
  NBDefs      \
  NBStructs   \
  NBSWIs      \
  NBVersion   \

OBJS =        \
 NBInternal   \
 NB_Dirs      \
 NB_Files     \
 NB_Flags     \
 NB_Gadgets   \
 NB_Misc      \
 NB_MsgTran   \
 NB_Objects   \
 NB_String    \
 NB_TaskWin   \
 NB_Version   \
 NB_Window    \
 NBLibVersion \
 NB_HTML      \

SOURCES_TO_SYMLINK = $(wildcard c++/*) VersionNum

include CLibrary

# Dynamic dependencies:
